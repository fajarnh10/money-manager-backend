JAR_HOME="/data/apps/jars/mocoex/moneyManager"
JAVA_HOME="/opt/jdk1.8.0_271/bin"

echo "Starting Apps ... "
echo

cd ${JAVA_HOME}
java -server -Xms512m -Xmx1g -XX:MaxMetaspaceSize=1g -Djasypt.encryptor.password=moneymanagerdatabase -jar ${JAR_HOME}/money-manager-backend-1.0.0.jar