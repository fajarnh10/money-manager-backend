package com.mocoex.moneyManager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mocoex.constant.ConstantData;
import com.mocoex.model.moneyManager.ReqInsertIncomeModel;
import com.mocoex.model.moneyManager.RespIncomeDataModel;
import com.mocoex.model.moneyManager.RespInsertIncomeModel;
import com.mocoex.moneyManager.service.IncomeService;

@RestController
@RequestMapping("/v1/income")
public class IncomeController extends BaseController {
	
	@Autowired
	private IncomeService incomeService;
	
	@Autowired
	private ObjectMapper mapper = new ObjectMapper();
	
	@RequestMapping(value = "/select", method=RequestMethod.GET)
	public  @ResponseBody RespIncomeDataModel getIncomeRangeById(
			@RequestParam(value="clientId", required = false) String clientId,
			@RequestParam(value="startId", required = false, defaultValue = ConstantData.C_ZERO) String startId,
			@RequestParam(value="limit", required = false, defaultValue = ConstantData.C_ZERO) String limit){

		logger.info("Incomming get income range by id with parameters startId ["+ startId +"], endId ["+ limit +"]");
		
		RespIncomeDataModel response = new RespIncomeDataModel();
		
		try {
			response = incomeService.getIncomeRangeById(clientId, startId, limit);
			mapper.writeValueAsString("Response get income range by id : " + response);
		} catch (Exception e) {
			logger.error("Get income range by id API error : ",e);
		}
		
		return response;
	}
	
	@RequestMapping(value = "/insert", method=RequestMethod.POST)
	public  @ResponseBody RespInsertIncomeModel insertIncome(
			@RequestBody(required = false) ReqInsertIncomeModel request) {
		
		logger.info("Incoming insert ["+ (request != null ? request.getIncomeData().size():0) +"] data income for client " + request.getClientId());
		
		RespInsertIncomeModel response = new RespInsertIncomeModel();
		
		try {
			response = incomeService.insertIncome(request);
		} catch (Exception e) {
			logger.error("Insert income API error : ",e);
		}
		
		return response;
	}
}
