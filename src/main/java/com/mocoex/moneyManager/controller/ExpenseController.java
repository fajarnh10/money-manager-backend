package com.mocoex.moneyManager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mocoex.constant.ConstantCode;
import com.mocoex.constant.ConstantData;
import com.mocoex.model.moneyManager.ReqInsertExpenseModel;
import com.mocoex.model.moneyManager.RespExpenseDataModel;
import com.mocoex.model.moneyManager.RespInsertExpenseModel;
import com.mocoex.moneyManager.service.ExpenseService;

@RestController
@RequestMapping("/v1/expense")
public class ExpenseController extends BaseController {
	
	@Autowired
	private ExpenseService expenseService;
	
	@Autowired
	private ObjectMapper mapper = new ObjectMapper();
	
	@RequestMapping(value = "/select", method=RequestMethod.GET)
	public @ResponseBody RespExpenseDataModel getExpenseRangeById(
			@RequestParam(value="clientId", required = false) String clientId,
			@RequestParam(value="startId", required = false, defaultValue = ConstantData.C_ZERO) String startId,
			@RequestParam(value="limit", required = false, defaultValue = ConstantData.C_ZERO) String limit){
		
		logger.info("Incomming get expense range by id with parameters startId ["+ startId +"], endId ["+ limit +"]");
		
		RespExpenseDataModel response = new RespExpenseDataModel();
		response.setResponseCode(ConstantCode.C_GENERAL_ERROR_CODE);
		response.setResponseMessage(ConstantCode.C_GENERAL_ERROR_MESSAGE);
		
		try {
			response = expenseService.getExpenseRangeById(clientId, startId, limit);
			mapper.writeValueAsString(response);
		} catch (Exception e) {
			logger.error("Get expense range by id API error : ",e);
		}
		
		return response;
	}
	
	@RequestMapping(value = "/insert", method=RequestMethod.POST)
	public  @ResponseBody RespInsertExpenseModel addExpense(
			@RequestBody(required = false) ReqInsertExpenseModel request) {
		
		logger.info("Incoming insert ["+ (request != null ? request.getExpenseData().size():0) +"] data expense ");
		
		RespInsertExpenseModel response = new RespInsertExpenseModel();
		
		try {
			response = expenseService.insertExpense(request);
		} catch (Exception e) {
			logger.error("Insert expense API error : ",e);
		}
		
		return response;
	}
	
}
