package com.mocoex.moneyManager.service;

import com.mocoex.model.moneyManager.ReqInsertExpenseModel;
import com.mocoex.model.moneyManager.RespExpenseDataModel;
import com.mocoex.model.moneyManager.RespInsertExpenseModel;

public interface ExpenseService {
	
	public RespExpenseDataModel getExpenseRangeById(String clientId, String startId, String limit);
	
	public RespInsertExpenseModel insertExpense(ReqInsertExpenseModel reqInsertExpenseModel);

}
