package com.mocoex.moneyManager.service;

import java.util.List;

import com.mocoex.model.moneyManager.UserModel;

public interface UserService {

	public List<UserModel> getAllUser();
	
}
