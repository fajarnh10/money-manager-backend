package com.mocoex.moneyManager.service;

import com.mocoex.model.moneyManager.ReqInsertIncomeModel;
import com.mocoex.model.moneyManager.RespIncomeDataModel;
import com.mocoex.model.moneyManager.RespInsertIncomeModel;

public interface IncomeService {

	public RespIncomeDataModel getIncomeRangeById(String clientId, String startId, String limit);
	
	public RespInsertIncomeModel insertIncome(ReqInsertIncomeModel reqInsertIncomeModel);
	
}
