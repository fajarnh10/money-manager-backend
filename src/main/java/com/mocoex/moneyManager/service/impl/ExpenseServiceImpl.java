package com.mocoex.moneyManager.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mocoex.constant.ConstantCode;
import com.mocoex.constant.ConstantData;
import com.mocoex.model.moneyManager.ExpenseModel;
import com.mocoex.model.moneyManager.ReqInsertExpenseModel;
import com.mocoex.model.moneyManager.RespExpenseDataModel;
import com.mocoex.model.moneyManager.RespInsertExpenseModel;
import com.mocoex.moneyManager.dao.ExpenseDao;
import com.mocoex.moneyManager.service.BaseService;
import com.mocoex.moneyManager.service.ExpenseService;

@Service
public class ExpenseServiceImpl extends BaseService implements ExpenseService{
	
	@Autowired
	private ExpenseDao expenseDao;
	
	@Override
	public RespExpenseDataModel getExpenseRangeById(String clientId, String startId, String limit) {
		
		List<ExpenseModel> expenseList = new ArrayList<ExpenseModel>();
		RespExpenseDataModel response = new RespExpenseDataModel();
		response.setResponseCode(ConstantCode.C_GENERAL_ERROR_CODE);
		response.setResponseMessage(ConstantCode.C_GENERAL_ERROR_MESSAGE);
		
		try {
			if (clientId != null && !clientId.isEmpty()) {
				if (Integer.parseInt(startId) <= 0) {
					startId = ConstantData.C_ZERO;
				}
				
				if (Integer.parseInt(limit) <= 0) {
					limit = String.valueOf(Integer.parseInt(startId) + 20);
				}
				
				if (Integer.parseInt(startId) <= 0) {
					startId = ConstantData.C_ZERO;
				}
				
				if (Integer.parseInt(limit) <= 0) {
					limit = String.valueOf(Integer.parseInt(startId) + 20);
				}
				
				try {
					expenseList = expenseDao.getExpenseRangeById(clientId, Integer.parseInt(startId), Integer.parseInt(limit));
				} catch (Exception e) {
					logger.error("Error when get data : ",e);
				}
				
				response.setExpenseData(expenseList);
				response.setResponseCode(ConstantCode.C_GENERAL_SUCCESS_CODE);
				response.setResponseMessage(ConstantCode.C_GENERAL_SUCCESS_MESSAGE);
			}else {
				response.setResponseCode(ConstantCode.C_MONEY_MANAGEMENT_ERROR_CLIENT_ID_NULL_CODE);
				response.setResponseMessage(ConstantCode.C_MONEY_MANAGEMENT_ERROR_CLIENT_ID_NULL_MESSAGE);
			}
		} catch (Exception e) {
			logger.error("Error income service, get income range by id : ",e);
		}
		
		return response;
	}

	@Override
	public RespInsertExpenseModel insertExpense(ReqInsertExpenseModel reqInsertExpenseModel) {
		RespInsertExpenseModel response = new RespInsertExpenseModel();
		response.setResponseCode(ConstantCode.C_GENERAL_ERROR_CODE);
		response.setResponseMessage(ConstantCode.C_GENERAL_ERROR_MESSAGE);
		
		try {
			if ((reqInsertExpenseModel != null ? reqInsertExpenseModel.getExpenseData().size():0) != 0) {
				if (validationInsertIncome(reqInsertExpenseModel.getExpenseData())) {
					try {
						expenseDao.insertExpense(reqInsertExpenseModel.getExpenseData(), reqInsertExpenseModel.getClientId());
						response.setResponseCode(ConstantCode.C_GENERAL_SUCCESS_CODE);
						response.setResponseMessage(ConstantCode.C_GENERAL_SUCCESS_MESSAGE);
					} catch (Exception e) {
						response.setResponseCode(ConstantCode.C_MONEY_MANAGEMENT_FAILED_INSERT_DATA_CODE);
						response.setResponseMessage(ConstantCode.C_MONEY_MANAGEMENT_FAILED_INSERT_DATA_MESSAGE);
						logger.error("Error insert data : ",e);
					}
				}else {
					response.setResponseCode(ConstantCode.C_MONEY_MANAGEMENT_INVALID_INSERT_DATA_CODE);
					response.setResponseMessage(ConstantCode.C_MONEY_MANAGEMENT_INVALID_INSERT_DATA_MESSAGE);
				}
			}else {
				response.setResponseCode(ConstantCode.C_MONEY_MANAGEMENT_ERROR_DATA_NULL_CODE);
				response.setResponseMessage(ConstantCode.C_MONEY_MANAGEMENT_ERROR_DATA_NULL_MESSAGE);
			}
		} catch (Exception e) {
			logger.error("General error service insertExpense : ",e);
		}
		
		return response;
	}
	
	public boolean validationInsertIncome(List<ExpenseModel> expenseModelList) {
		boolean returnValue = true;
		
		for (int i = 0; i < expenseModelList.size(); i++) {
			returnValue = isValidParamInsert(expenseModelList.get(i));
			if (!returnValue) {
				break;
			}
		}
		
		return returnValue;
	}
	
	public boolean isValidParamInsert(ExpenseModel expenseModel) {
		boolean returnValue = true;
		
		if (expenseModel.getAccount() == null || expenseModel.getAccount().isEmpty() ||
			expenseModel.getCategory() == null || expenseModel.getCategory().isEmpty() ||
			expenseModel.getAmount() == null || expenseModel.getAmount().intValue() <= 0) {
			
			returnValue = false;
			
		}
		return returnValue;
	}

}
