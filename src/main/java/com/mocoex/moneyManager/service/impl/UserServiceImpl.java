package com.mocoex.moneyManager.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mocoex.model.moneyManager.UserModel;
import com.mocoex.moneyManager.dao.UserDao;
import com.mocoex.moneyManager.service.BaseService;
import com.mocoex.moneyManager.service.UserService;

@Service
public class UserServiceImpl extends BaseService implements UserService{

	@Autowired
	private UserDao userDao;
	
	@Override
	public List<UserModel> getAllUser() {
		List<UserModel> response = new ArrayList<UserModel>();
		try {
			response = userDao.getAllUser();
		} catch (Exception e) {
			logger.error("Get all user general error : ",e);
		}
		return response;
	}

}
