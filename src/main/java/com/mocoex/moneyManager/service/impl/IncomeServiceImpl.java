package com.mocoex.moneyManager.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mocoex.checker.Checker;
import com.mocoex.constant.ConstantCode;
import com.mocoex.constant.ConstantData;
import com.mocoex.model.moneyManager.IncomeModel;
import com.mocoex.model.moneyManager.ReqInsertIncomeModel;
import com.mocoex.model.moneyManager.RespIncomeDataModel;
import com.mocoex.model.moneyManager.RespInsertIncomeModel;
import com.mocoex.moneyManager.dao.IncomeDao;
import com.mocoex.moneyManager.service.BaseService;
import com.mocoex.moneyManager.service.IncomeService;

@Service
public class IncomeServiceImpl extends BaseService implements IncomeService{

	@Autowired
	private IncomeDao incomeDao;
	
	@Override
	public RespIncomeDataModel getIncomeRangeById(String clientId, String startId, String limit) {
		
		List<IncomeModel> incomeList = new ArrayList<IncomeModel>();
		RespIncomeDataModel response = new RespIncomeDataModel();
		response.setResponseCode(ConstantCode.C_GENERAL_ERROR_CODE);
		response.setResponseMessage(ConstantCode.C_GENERAL_ERROR_MESSAGE);
		
		try {
			if (clientId != null && !clientId.isEmpty()) {
				if (!Checker.numericValue(startId)) {
					startId = ConstantData.C_ZERO;
				}
				
				if (!Checker.numericValue(limit)){
					limit = String.valueOf(Integer.parseInt(startId) + 20);
				}
				
				if (Integer.parseInt(startId) <= 0) {
					startId = ConstantData.C_ZERO;
				}
				
				if (Integer.parseInt(limit) <= 0) {
					limit = String.valueOf(Integer.parseInt(startId) + 20);
				}
				
				try {
					incomeList = incomeDao.getIncomeRangeById(clientId, Integer.parseInt(startId), Integer.parseInt(limit));
				} catch (Exception e) {
					logger.error("Error when get data : ",e);
				}
				
				response.setIncomeData(incomeList);
				response.setResponseCode(ConstantCode.C_GENERAL_SUCCESS_CODE);
				response.setResponseMessage(ConstantCode.C_GENERAL_SUCCESS_MESSAGE);
			} else {
				response.setResponseCode(ConstantCode.C_MONEY_MANAGEMENT_ERROR_CLIENT_ID_NULL_CODE);
				response.setResponseMessage(ConstantCode.C_MONEY_MANAGEMENT_ERROR_CLIENT_ID_NULL_MESSAGE);
			}
		} catch (Exception e) {
			logger.error("General error service getIncomeRangeById : ",e);
		}
		
		return response;
	}

	@Override
	public RespInsertIncomeModel insertIncome(ReqInsertIncomeModel reqInsertIncomeModel) {
		RespInsertIncomeModel response = new RespInsertIncomeModel();
		response.setResponseCode(ConstantCode.C_GENERAL_ERROR_CODE);
		response.setResponseMessage(ConstantCode.C_GENERAL_ERROR_MESSAGE);
		
		try {
			if (reqInsertIncomeModel.getClientId() != null && !reqInsertIncomeModel.getClientId().isEmpty()) {
				if ((reqInsertIncomeModel != null ? reqInsertIncomeModel.getIncomeData().size():0) != 0) {
					if (validationInsertIncome(reqInsertIncomeModel.getIncomeData())) {
						try {
							incomeDao.insertIncome(reqInsertIncomeModel.getIncomeData(), reqInsertIncomeModel.getClientId());
							response.setResponseCode(ConstantCode.C_GENERAL_SUCCESS_CODE);
							response.setResponseMessage(ConstantCode.C_GENERAL_SUCCESS_MESSAGE);
						} catch (Exception e) {
							response.setResponseCode(ConstantCode.C_MONEY_MANAGEMENT_FAILED_INSERT_DATA_CODE);
							response.setResponseMessage(ConstantCode.C_MONEY_MANAGEMENT_FAILED_INSERT_DATA_MESSAGE);
							logger.error("Error insert data : ",e);
						}
					}else {
						response.setResponseCode(ConstantCode.C_MONEY_MANAGEMENT_INVALID_INSERT_DATA_CODE);
						response.setResponseMessage(ConstantCode.C_MONEY_MANAGEMENT_INVALID_INSERT_DATA_MESSAGE);
					}
				}else {
					response.setResponseCode(ConstantCode.C_MONEY_MANAGEMENT_ERROR_DATA_NULL_CODE);
					response.setResponseMessage(ConstantCode.C_MONEY_MANAGEMENT_ERROR_DATA_NULL_MESSAGE);
				}
			} else {
				response.setResponseCode(ConstantCode.C_MONEY_MANAGEMENT_ERROR_CLIENT_ID_NULL_CODE);
				response.setResponseMessage(ConstantCode.C_MONEY_MANAGEMENT_ERROR_CLIENT_ID_NULL_MESSAGE);
			}
		} catch (Exception e) {
			logger.error("General error service insertIncome : ",e);
		}
		
		return response;
	}
	
	public boolean validationInsertIncome(List<IncomeModel> incomeModelList) {
		boolean returnValue = true;
		
		for (int i = 0; i < incomeModelList.size(); i++) {
			returnValue = isValidParamInsert(incomeModelList.get(i));
			if (!returnValue) {
				break;
			}
		}
		
		return returnValue;
	}
	
	public boolean isValidParamInsert(IncomeModel incomeModel) {
		boolean returnValue = true;
		
		if (incomeModel.getAccount() == null || incomeModel.getAccount().isEmpty() ||
			incomeModel.getCategory() == null || incomeModel.getCategory().isEmpty() ||
			incomeModel.getAmount() == null || incomeModel.getAmount().intValue() <= 0) {
			
			returnValue = false;
			
		}
		
		return returnValue;
	}
	
}
