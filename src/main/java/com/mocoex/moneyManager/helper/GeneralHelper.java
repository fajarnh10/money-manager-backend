package com.mocoex.moneyManager.helper;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class GeneralHelper {
	
	public static String bCryptEncoder(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(password);
	}
	
	public static boolean bCryptMatcher(String password, String userPassword) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();  
		return passwordEncoder.matches(password, userPassword);
	}
	
}
