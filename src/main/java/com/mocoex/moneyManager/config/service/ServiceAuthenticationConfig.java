package com.mocoex.moneyManager.config.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.mocoex.model.moneyManager.UserModel;
import com.mocoex.moneyManager.service.UserService;

@Configuration
@EnableWebSecurity
public class ServiceAuthenticationConfig extends WebSecurityConfigurerAdapter {
	
	 @Autowired
	 private ServiceAuthenticationProvider securityProvider;
	 
	 @Autowired
	 protected Environment env;
	 
	 @Autowired
	 private UserService userService;
	 
	 @Override
	 protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests()
				.anyRequest().authenticated()
				.and()
				.httpBasic()
				.authenticationEntryPoint(securityProvider);
	 }
	 
	 @Override
	 public void configure(AuthenticationManagerBuilder auth) throws Exception {
		List<UserModel> userList = new ArrayList<UserModel>();
		userList = userService.getAllUser();
		if (userList.size() > 0) {
			for (int i = 0; i < userList.size(); i++) {
	    		auth
	    		.inMemoryAuthentication()
	    		.passwordEncoder(passwordEncoder())
	    		.withUser(userList.get(i).getUsername())
	    		.password(userList.get(i).getPassword())
	    		.roles("USER");
    		}
		}else {
			String user = env.getProperty("money.manager.auth.id");
	    	String pwd = env.getProperty("money.manager.auth.pwd");

    		auth
    		.inMemoryAuthentication()
    		.passwordEncoder(passwordEncoder())
    		.withUser(user)
    		.password(pwd)
    		.roles("USER");

		}
	 }
	 
	 @Bean
	 public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
	 }

}
