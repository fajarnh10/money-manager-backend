package com.mocoex.moneyManager.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mocoex.model.moneyManager.UserModel;

@Mapper
public interface UserDao {
	
	public List<UserModel> getAllUser();
	
	public UserModel getUserByUsernameAndPassword(
			@Param("username") String username,
			@Param("password") String password);
	
}
