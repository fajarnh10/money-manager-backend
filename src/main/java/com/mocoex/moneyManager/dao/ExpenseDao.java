package com.mocoex.moneyManager.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mocoex.model.moneyManager.ExpenseModel;

@Mapper
public interface ExpenseDao {

	public List<ExpenseModel> getExpenseRangeById(
			@Param("clientId") String clientId, 
			@Param("startId") Integer startId, 
			@Param("limit") Integer limit);
	
	public void insertExpense(
			List<ExpenseModel> expenseModelList,
			@Param("clientId") String clientId);
	
}
