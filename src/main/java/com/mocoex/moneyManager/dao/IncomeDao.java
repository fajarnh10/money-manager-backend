package com.mocoex.moneyManager.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mocoex.model.moneyManager.IncomeModel;

@Mapper
public interface IncomeDao {

	public List<IncomeModel> getIncomeRangeById(
			@Param("clientId") String clientId,
			@Param("startId") Integer startId, 
			@Param("limit") Integer limit);
	
	public void insertIncome(
			List<IncomeModel> incomeModelList,
			@Param("clientId") String clientId);
	
}
