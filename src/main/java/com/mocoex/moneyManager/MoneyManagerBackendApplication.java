package com.mocoex.moneyManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

@SpringBootApplication
@EnableEncryptableProperties
public class MoneyManagerBackendApplication implements CommandLineRunner {
	
	protected static final Logger logger = LoggerFactory.getLogger(MoneyManagerBackendApplication.class);

	public static void main(String[] args) {
		try {
			SpringApplication.run(MoneyManagerBackendApplication.class, args);
		} catch (Exception e) {
			logger.error("Error running money manager backend application : ",e);
		}
	}
	
	@Override
	public void run(String... args) throws Exception {
	}

}
